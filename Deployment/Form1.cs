﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Deployment
{
    public partial class Form1 : Form
    {
       
        public Form1()
        {
            InitializeComponent();
           
        }


        private void btnBrowseSource_Click(object sender, EventArgs e)
        {
           
            var result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtSource.Text = folderBrowserDialog1.SelectedPath;
            }  
            
        }

        private void btnBrowseDestination_Click(object sender, EventArgs e)
        {
            var result = folderBrowserDialog1.ShowDialog();
            if (result == DialogResult.OK)
            {
                txtDestination.Text = folderBrowserDialog1.SelectedPath;
            }

        }

        private void btnCopy_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;

            var str = Application.StartupPath;
            var strFileNameOnly = "Copied_Files_" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
            string fileName = str + "\\" + strFileNameOnly;


            if (txtDestination.Text.Length == 0 || txtSource.Text.Length == 0)
            {
                this.Cursor = Cursors.Default;
                MessageBox.Show("Source and destination can't be empty", "Alert");
                return;
            }
            string[] files = txtFiles.Text.Split( '\n');
            lvResult.Items.Clear();
            WriteFileNameThatSuccess(fileName, "--Start: " + DateTime.Now.ToString("yyyy-MM-dd HH:mm"));
            foreach (var f in files)
            {
               
                
                try
                {
                    if (f.Trim().Length == 0)
                    {
                        continue;
                    }
                    Copy(txtSource.Text, txtDestination.Text, f.Trim());
                    var l = new ListViewItem();
                    l.Text = txtSource.Text + "\\" + f.Trim();
                    l.SubItems.Add(txtDestination.Text + "\\"+ f.Trim());
                    l.SubItems.Add("Ok");
                    l.BackColor = Color.Green;

                    lvResult.Items.Add(l);

                    WriteFileNameThatSuccess(fileName, f.Trim());

                }
                catch (Exception ex)
                {
                    var l = new ListViewItem();
                    l.Text = txtSource.Text + "\\" + f.Trim();
                    l.SubItems.Add(txtDestination.Text + "\\" + f.Trim());
                    l.SubItems.Add(ex.Message);
                    l.BackColor = Color.Red;
                    lvResult.Items.Add(l);
                }


            }

            WriteFileNameThatSuccess(fileName, "--End\n");
            Cursor = Cursors.Default;

            string result = "Done!, \n" + "Files that had been copied is logged in " + strFileNameOnly;

            MessageBox.Show(result, "Info");
        }

        private void Copy(string sourcePath, string targetPath, string folderOrFile)
        {

         



            if (Directory.Exists(sourcePath + "\\" + folderOrFile))
            {

              

                string[] files = System.IO.Directory.GetFiles(sourcePath + "\\" + folderOrFile);

                //Copy the files and overwrite destination files if they already exist.
                foreach (string s in files)
                {
                    CreateFolderDestination(targetPath, folderOrFile);
                    //Use static Path methods to extract only the file name from the path.
                    var fileName = Path.GetFileName(s);
                    File.Copy(sourcePath+"\\" + folderOrFile  + fileName, targetPath + "\\" + folderOrFile + fileName, true);
                }

                var dics = Directory.GetDirectories(sourcePath + "\\" + folderOrFile);
                foreach (string strFolder in dics)
                {
                    var temp = strFolder.Replace(sourcePath, "");
                    Copy(sourcePath + "\\" , targetPath + "\\" , temp + "\\");
                }

                
            }
            else if (File.Exists(sourcePath + "\\" + folderOrFile))
            {
                CreateFolderDestination(targetPath, folderOrFile);

                File.Copy(sourcePath + "\\" + folderOrFile, targetPath + "\\" + folderOrFile, true);
            }

            else
            {
                throw new Exception("Source file is not exist " + sourcePath + "\\" + folderOrFile);
            }


           
        }

        private static void CreateFolderDestination(string targetPath, string folderOrFile)
        {
            var folder = Path.GetDirectoryName(folderOrFile);

            if (!Directory.Exists(targetPath + "\\" + folder))
            {
                Directory.CreateDirectory(targetPath + "\\" + folder);

            }
          
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            var str = Application.StartupPath;
            txtDestination.Text = str + "\\Deploy_" + DateTime.Now.ToString("yyyyMMdd") + "\\Webfiles\\astrid";

            // Write file contents on console.   
            ReadSourceFolder();

         



        }

        private void ReadFileToBeCopied()
        {
            var str = Application.StartupPath;
            string fileName = str + "\\files.txt";

            var fi = new FileInfo(fileName);

            if (fi.Exists)
            {
                using (StreamReader sr = File.OpenText(fileName))
                {
                    var sb = new StringBuilder();
                    string s;
                    while ((s = sr.ReadLine()) != null)
                    {
                        sb.AppendLine(s);

                    }
                    sr.Close();
                    txtFiles.Text = sb.ToString();
                }

            }
            else
            {
                MessageBox.Show(fileName + " is not exist!");
            }
        }

        private void ReadSourceFolder()
        {
            var str = Application.StartupPath;
            var fileName = str + "\\source.txt";

            var fi = new FileInfo(fileName);

            if (fi.Exists)
            {
                using (StreamReader sr = File.OpenText(fileName))
                {
                    string s = "";
                    while ((s = sr.ReadLine()) != null)
                    {
                        lvHistory.Items.Add(s);
                    }
                    sr.Close();
                }
            }
        }

        private void WriteFileNameThatSuccess(string fileDestination, string data)
        {
           
            var fi = new FileInfo(fileDestination);
            if (!fi.Exists)
            {
               var c = fi.Create();
                c.Close();
            }

            using (StreamWriter sw = fi.AppendText())
            {

                sw.WriteLine(data);
                sw.Close();
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtSource.Text.Trim().Length == 0)
            {
                MessageBox.Show("Input data Source first");
                return;
            }

            var str = Application.StartupPath;
            string fileName = str + "\\source.txt";
            var fi = new FileInfo(fileName);

            try
            {
                // Check if file already exists. If yes, delete it.     
                if (!fi.Exists)
                {
                    var file = fi.Create();
                    file.Close();

                }
                bool isExit = false;

                using (StreamReader sr = File.OpenText(fileName))
                {
                    string s = "";
                   
                    while ((s = sr.ReadLine()) != null && !isExit)
                    {
                        if (s == txtSource.Text)
                        {
                            isExit = true;
                        }
                        
                    }
                    sr.Close();
                }


                if (!isExit)
                {
                    using (StreamWriter sw = fi.AppendText())
                    {

                        sw.WriteLine(txtSource.Text);
                        sw.Close();
                    }
                    lvHistory.Items.Add(txtSource.Text);
                }
                else
                {
                    MessageBox.Show("Data already exist");
                }
            }
            catch (Exception Ex)
            {
                MessageBox.Show(Ex.Message);
            }
        }

        private void lvHistory_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((ListBox) sender).SelectedItem != null)
                txtSource.Text = lvHistory.SelectedItem.ToString();
        }

        private void btnRead_Click(object sender, EventArgs e)
        {
            ReadFileToBeCopied();
        }

    }
}
